//********************LES IMPORTS ******************** */
import { Sphere } from "./models/Sphere.js"
import { Transition1_to_2 } from "./Transitions/1_to_2.js"
import { Scene1 } from "./Scene/Scene1.js"
import { Timeline } from "./Timeline.js"
//***************VARIABLES*******************/
export let scene, camera, renderer, light1, light2, light3, light4, sphere, plane, spotLightHelper, target4;
export let ordi = {};
export let spheres = [];
export let plans = []
let textProjets = ["Application E-commerce", "Application de gestion d’une agence immobilière", "Application de gestion de contentieux", "Fonctionnalité météo", "Application E-commerce", "Application de gestion d’une agence immobilière", "Application de gestion de contentieux", "Fonctionnalité météo"]
let imagesProjets = ["E-shop.jpg", "immobilier.jpg", "justice.jpg", "meteo.jpg", "E-shop.jpg", "immobilier.jpg", "justice.jpg", "meteo.jpg"]
let spaceBetween = Math.PI / 4;
let event = {}
let theta = 0;
let dragActivate = false
let gammeCouleur = [0xb653c4, 0x120466, 0x5f16e0, 0x2f069b, 0x5f2495, 0xe2d5ed, 0x070443, 0x3f167c, 0x8184b1, 0x48447c]

//***************CREATION D'OBJET*******************/
let createGeometry = () => {

    //geometry de tout les panneaux
    let geometry = new THREE.BoxBufferGeometry(13, 8, 0.1);
    // création de 8 panneaux
    for (let i = 0; i < 8; i++) {

        let texture = new THREE.TextureLoader().load("./assets/image/" + imagesProjets[i])
        let material = new THREE.MeshPhongMaterial({
            color: 0x000000,
            shininess: 100,
            map: texture
        })

        // caractéristiques de tout les panneaux
        sphere = new THREE.Mesh(geometry, material)
        sphere.position.x = Math.cos(theta) * 25;
        sphere.position.z = Math.sin(theta) * 25;
        sphere.position.y = randomInRange(-11, 0);
        sphere.castShadow = true;
        sphere.receiveShadow = false;
        let sphereClass = new Sphere(sphere, theta, texture)
        theta += spaceBetween;

        // ajout des panneaux à la scene et au tableau des panneaux  
        scene.add(sphere)
        spheres.push(sphereClass)
    }

    //création de l'ordi
    const loader = new THREE.GLTFLoader()
    loader.load('./pc3dmodel/scene.gltf', result => {
        ordi = result.scene.children[0];
        ordi.position.set(0, -33, 0);
        ordi.scale.set(5, 5, 5)
        ordi.castShadow = true;
        ordi.receiveShadow = false;
        scene.add(ordi);

    });



}




//**************METHODE NOMMBRE ALEATOIRE ENTRE DEUX NOMBRE*******************/
let randomInRange = (from, to) => {
    let random = Math.random() * (to - from);
    return from + random
}


//***************METHODE CREATION PLAN*******************/
//todo: cette méthode doit-etre générique car pour chaque scene il y a un plan a créer
let createPlane1 = () => {
    let x = 0;
    let geometry = new THREE.BoxBufferGeometry(75, 5, 75);
    for (let i = 0; i < 3; i++) {

        let color = gammeCouleur[Math.floor(randomInRange(0, 10))]
        let material = new THREE.MeshPhongMaterial({
            color: color,
            shininess: 300,
            side: THREE.DoubleSide
        })
        plane = new THREE.Mesh(geometry, material)
        plane.position.set(x, -20, 0)
        x += 75
        plane.receiveShadow = true;
        plans.push(plane)
        scene.add(plane)

    }
    // création du troisième plan
    let texture = new THREE.TextureLoader().load("./assets/image/parcours.PNG")
    geometry = new THREE.BoxBufferGeometry(75, 150, 5);

    let color = gammeCouleur[Math.floor(randomInRange(0, 10))]
    let material = new THREE.MeshPhongMaterial({
        color: 0xffffff,
        map: texture,
        shininess: 100,
        side: THREE.DoubleSide,

    })
    plane = new THREE.Mesh(geometry, material)
    plane.position.set(75, -94, -75 / 2)

    plans.push(plane)
    scene.add(plane)

}


//***************METHODE INITIALISATION*******************/
// doit-on créer plusieurs lumière ou une lumiere pour chaque scène?
let init = function() {

    scene = new THREE.Scene()
    scene.background = new THREE.Color(0x0b0b0b);

    camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 1, 1000)
    camera.position.set(0, 5, 40);

    // instanciation et initialisation
    ordi.position = {};
    camera.lookAt(camera.position.x, camera.position.y, camera.position.z)

    // lumière
    light2 = new THREE.SpotLight(0xffffff, 1)
    light2.position.set(0, 0, 40)
    light2.angle = Math.PI / 3;
    light2.penumbra = 0.05;
    light2.decay = 2;
    light2.distance = 200;
    light2.castShadow = true;
    light2.shadow.mapSize.width = 512;
    light2.shadow.mapSize.height = 512;
    light2.shadow.camera.near = 0.5;
    light2.shadow.camera.far = 500;
    light2.shadow.focus = 1;
    scene.add(light2)


    light3 = new THREE.SpotLight(0xffffff, 2)
    light3.position.set(0, 100, 0)
    light3.angle = Math.PI / 20;
    light3.penumbra = 0.05;
    light3.decay = 2;
    light3.distance = 500;
    light3.castShadow = true;
    light3.shadow.mapSize.width = 512;
    light3.shadow.mapSize.height = 512;
    light3.shadow.camera.near = 0.5;
    light3.shadow.camera.far = 500;
    light3.shadow.focus = 1;
    scene.add(light3)

    light4 = new THREE.SpotLight(0xffffff, 1.5)
    light4.position.set(75, 15, 50)
    light4.angle = Math.PI / 5;
    light4.penumbra = 0.05;
    light4.decay = 2;
    light4.distance = 500;
    light4.castShadow = true;
    light4.shadow.mapSize.width = 512;
    light4.shadow.mapSize.height = 512;
    light4.shadow.camera.near = 0.5;
    light4.shadow.camera.far = 500;
    light4.shadow.focus = 1;
    target4 = new THREE.Object3D()
    target4.position.x = 75;
    target4.position.y = 15;
    target4.position.z = 40;
    scene.add(target4)
    scene.add(light4)

    //les méthodes appelés
    createGeometry();
    createPlane1();
    Scene1.avancerReculer(event);

    //eventListener
    document.addEventListener("mousemove", (e) => {
        // activate signifie qu'on est entrain de drag voir les eventlistener mousedown et mouseup
        if (dragActivate) {
            Timeline.dispatchScene(e);
        }

    })

    document.addEventListener("mousedown", (e) => {
        dragActivate = true

    })

    document.addEventListener("mouseup", (e) => {
        dragActivate = false

    })

    //rendu
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    document.body.appendChild(renderer.domElement);


};



//***************METHODE RAFFRAICHISSEMENT*******************/
let mainloop = function() {

    renderer.render(scene, camera);
    requestAnimationFrame(mainloop);

}


//***************APPEL DE METHODE*******************/
init();
mainloop();