import { camera, light4, target4 } from "../main.js"
import { Transition1_to_2 } from "../Transitions/1_to_2.js"
import { plans } from "../main.js"
let add = 1;

export class Scene2 {

    static descendreMonter(e) {
        // changer l'angle de vue de la caméra
        camera.lookAt(camera.position.x, camera.position.y, camera.position.z)
        console.log("je suis dans la méthode descendreMonter")
        if (camera.position.y <= 5 && e.movementX < 0) {
            camera.position.y -= add
            light4.position.y -= add
            target4.position.y -= add
            light4.target = target4
            plans[1].position.y -= add
            console.log("je descend")

        } else if (camera.position.y <= 5 && e.movementX > 0) {
            camera.position.y += add
            target4.position.y += add
            light4.position.y += add
            light4.target = target4
            plans[1].position.y += add

            console.log("je monte")


        } else if (camera.position.y >= 5 && e.movementX > 0) {
            camera.position.y = 5
                // light4.target.y = 2
            plans[1].position.y = -20
            console.log("j'appel la transition 1 à 2")
            Transition1_to_2.changerPlan(e);

        } else if (camera.position.y >= 5 && e.movementX < 0) {
            camera.position.y -= add
            target4.position.y -= add
            light4.position.y -= add
            light4.target = target4
            plans[1].position.y += add

            console.log("je monte")


        }


    }
}