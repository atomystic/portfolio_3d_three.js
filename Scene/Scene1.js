import { spheres } from "../main.js"
import { ordi } from "../main.js"
import { camera } from "../main.js"
let add, addOrdi;

export class Scene1 {

    static avancerReculer(e) {
        camera.lookAt(ordi.position)
        if (e.movementX < 0) {
            // déplacement ordi
            addOrdi = 0.015
                // déplacement panneaux
            add = 0.025
            spheres.forEach(sphere => {
                ordi.position.y += addOrdi
                let a = sphere.startTheta += add
                sphere.sphere.rotation.x = a + Math.PI / 2;
                sphere.sphere.rotation.y = a + Math.PI / 2;
                sphere.sphere.rotation.z = a + Math.PI / 2;
                sphere.sphere.position.x = Math.cos(a) * 25
                sphere.sphere.position.z = Math.sin(a) * 25


                if (sphere.sphere.position.x > -5 && sphere.sphere.position.x < 5 && sphere.sphere.position.z > 0) {
                    sphere.sphere.material.color = new THREE.Color(0xffffff);
                    sphere.sphere.scale.x = 1.5
                    sphere.sphere.scale.y = 1.5
                } else {
                    sphere.sphere.scale.x = 1
                    sphere.sphere.scale.y = 1
                    sphere.sphere.material.color = new THREE.Color(0x000000);
                }
            })

        } else if (e.movementX > 0 && spheres[0].startTheta >= 0) {


            spheres.forEach(sphere => {
                add = -0.025
                addOrdi = -0.015
                ordi.position.y += addOrdi
                let a = sphere.startTheta += add
                sphere.sphere.rotation.x = a + Math.PI / 2;
                sphere.sphere.rotation.y = a + Math.PI / 2;
                sphere.sphere.rotation.z = a + Math.PI / 2;
                sphere.sphere.position.x = Math.cos(a) * 25
                sphere.sphere.position.z = Math.sin(a) * 25



                if (sphere.sphere.position.x > -5 && sphere.sphere.position.x < 5 && sphere.sphere.position.z > 0) {
                    sphere.sphere.material.color = new THREE.Color(0xffffff);
                    sphere.sphere.scale.x = 1.5
                    sphere.sphere.scale.y = 1.5
                } else {
                    sphere.sphere.scale.x = 1
                    sphere.sphere.scale.y = 1
                    sphere.sphere.material.color = new THREE.Color(0x000000);
                }
            })
        }

    }
}