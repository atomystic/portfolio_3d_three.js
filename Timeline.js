import { camera, scene, ordi } from "./main.js"
import { Scene1 } from "./Scene/Scene1.js"
import { Scene2 } from "./Scene/Scene2.js"
import { Transition1_to_2 } from "./Transitions/1_to_2.js"
export class Timeline {

    static dispatchScene(e) {

        console.log(camera.position.x + " " + camera.position.y + " " + camera.position.z)
            //todo attention a changer de configuration en fonction des prochains changement
        if (camera.position.x <= 75 && camera.position.y >= 5 && camera.position.z >= 40) { // startheta<=math.pi

            console.log("j'appel  la méthode avancer reculer et le changement de plan")
            Scene1.avancerReculer(e); // attention startheta doit arreter d'ajouter quand on passe le 2**pi
            // todo faire une autre if pour ca et changer le point de vue de la camera
            Transition1_to_2.changerPlan(e);



        } else if (camera.position.x > 75 && camera.position.z >= 40 && camera.position.y >= (-145)) {

            console.log("j'appel  la méthode descendre")
            Scene2.descendreMonter(e);

        }

    }

}